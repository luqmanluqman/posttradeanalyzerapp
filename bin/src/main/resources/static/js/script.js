$(function () { 
	    var myChart = Highcharts.chart('container', {
        chart: {
            type: 'bar'
        },
        title: {
            text: 'Snacks Consumption of Citibankers Weekly'
        },
        subtitle: {
        	text: 'Created by Team B'
        },
        xAxis: {
            categories: ['Apples', 'Bananas', 'Durians', 'Kit Kat Bars', 'Waffles']
        },
        yAxis: {
            title: {
                text: 'Snacks eaten'
            }
        },
        series: [{
            name: 'Andy',
            data: [1, 0, 4, 2, 3]
        }, {
            name: 'Bobby',
            data: [5, 7, 3, 3, 8]
        }, {
            name: 'Cindy',
            data: [3, 1, 1, 1, 2]
        }]
    });
});

var chart1; // globally available
$(function() {
       chart1 = Highcharts.stockChart('container', {
         rangeSelector: {
            selected: 1
         },
         series: [{
            name: 'USD to EUR',
            data: usdtoeur // predefined JavaScript array
         }]
      });
   });

