package com.DatabaseHandling;

import java.util.ArrayList;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

import com.entities.ERByAccount;
import com.entities.ERByInterval;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.entities.ExecutionReport;

@Repository
@Transactional
public class DatabaseGenerator {

	@PersistenceContext
	private EntityManager entityManager;

	@Autowired
	private EntityManagerFactory emf;

	public void addReports(ArrayList<ExecutionReport> execReports)
	{
		entityManager = emf.createEntityManager();
		entityManager.getTransaction().begin();
		for(ExecutionReport report : execReports)
		{
			entityManager.persist(report);
		}
		entityManager.getTransaction().commit();
		entityManager.close();
	}

	public void addERByAccount(ArrayList<ERByAccount> erByAccount)
	{
		entityManager = emf.createEntityManager();
		entityManager.getTransaction().begin();
		for(ERByAccount er : erByAccount)
		{
			entityManager.persist(er);
		}
		entityManager.getTransaction().commit();
		entityManager.close();
	}

	public void addERByInterval(ArrayList<ERByInterval> erByInterval)
	{
		entityManager = emf.createEntityManager();
		entityManager.getTransaction().begin();
		for(ERByInterval er : erByInterval)
		{
			entityManager.persist(er);
		}
		entityManager.getTransaction().commit();
		entityManager.close();
	}
}
