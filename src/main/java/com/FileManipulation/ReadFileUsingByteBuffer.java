package com.FileManipulation;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.parsers.ExecutionReportFIXMessageParser;

@Component
public class ReadFileUsingByteBuffer
{
	@Autowired
	private ExecutionReportFIXMessageParser reportParser;

	int count = 0;
	static List<String> erfill = new ArrayList<String>();
	
	public void read(String filePath)
	{	
		try
		{		
			File file = new File(filePath);
			BufferedReader br = new BufferedReader(new FileReader(file));
			 
			String line;
			while ((line = br.readLine()) != null)
			{
				erfill.add(line);
				count++;
				if (count== 1000)
				{
					reportParser.parse(erfill);
					count = 0;
					erfill = new ArrayList<String>();
				}
			}
			
			if(erfill.size()>0)
			{
				reportParser.parse(erfill);
			}
			
			br.close();
		}
		
		catch (IOException e1)
		{
			e1.printStackTrace();
		}
	}
}