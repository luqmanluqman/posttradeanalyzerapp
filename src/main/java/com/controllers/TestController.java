package com.controllers;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;

//import java.util.HashMap;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
//@RequestMapping(value="/abc")
@CrossOrigin
public class TestController {
	// Get all aggregated broker commissions for every time interval.
		@RequestMapping(method=RequestMethod.GET, 
				        value="/aBBC", 
					    headers="Accept=application/json, application/xml, text/plain")
		public ArrayList<Integer> getAggrBrokerCommissions() {
			ArrayList<Integer> aaa =  new ArrayList<Integer>();
			aaa.add(10);
			aaa.add(12);
			return aaa;
		}
		
		@RequestMapping(method=RequestMethod.GET, 
		        value="/IOCHitRates1", 
			    headers="Accept=application/json, application/xml, text/plain")
public HashMap<String, Double> getIOCHitRates() {
	HashMap<String, Double> abc = new HashMap<String, Double>();
	abc.put("a", 1.0);
	abc.put("b", 2.0);
	abc.put("c", 3.0);
			return abc;
}		
}
