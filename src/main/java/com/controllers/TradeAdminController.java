package com.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.FileManipulation.ReadFileUsingByteBuffer;

@RestController
@CrossOrigin
public class TradeAdminController {
	
	@Autowired
	private ReadFileUsingByteBuffer readFileUsingByteBuffer;
	
	// Load data from log file to db.
	@RequestMapping(method=RequestMethod.GET, 
			        value="/tradeAdmin/{filePath}", 
				    headers="Accept=application/json, application/xml, text/plain")
	public void loadData(@PathVariable String filePath) {
		readFileUsingByteBuffer.read(filePath);
	}	
		
}