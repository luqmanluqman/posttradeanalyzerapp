package com.controllers;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.entities.ERByAccount;
import com.entities.ERByInterval;
import com.service.TradeAnalysisService;

@RestController
@CrossOrigin
public class TradeAnalysisController
{
	@Autowired
	private TradeAnalysisService service;
	
	// Get all aggregated broker commissions for every time interval.
	@RequestMapping(method=RequestMethod.GET, 
			        value="/aggrBrokerCommissions", 
				    headers="Accept=application/json")
	public HashMap<LocalDateTime, Double> getAggrBrokerCommissions() {
		HashMap<LocalDateTime, Double> hashMap = new HashMap<>();
		List<ERByInterval> erByInterval = new ArrayList<ERByInterval>();
		erByInterval = service.getBrokerCommissionByInterval();
		for (ERByInterval er : erByInterval) {
			hashMap.put(er.getSendingTime(), er.getBrokerCommission());
		}
		return hashMap;
	}	
	
	// Get all aggregated broker commissions for every time interval in milliseconds.
		@RequestMapping(method=RequestMethod.GET, 
				        value="/aggrBrokerCommissionsMillis", 
					    headers="Accept=application/json")
		public HashMap<Long, Double> getAggrBrokerCommissionsInMilliseconds() {
			HashMap<Long, Double> hashMap = new HashMap<>();
			List<ERByInterval> erByInterval = new ArrayList<ERByInterval>();
			erByInterval = service.getBrokerCommissionByInterval();
			for (ERByInterval er : erByInterval) {
				hashMap.put(millisecondsConverter(er.getSendingTime()), er.getBrokerCommission());
			}
			return hashMap;
		}	
	
	// Get aggregated broker commission for all data.
			@RequestMapping(method=RequestMethod.GET, 
							value="/aggrBrokerCommission", 
							headers="Accept=application/json")
		public HashMap<LocalDateTime, Double> getAggrBrokerCommission() {
			HashMap<LocalDateTime, Double> hashMap = new HashMap<>();	
			List<ERByInterval> erByInterval = new ArrayList<ERByInterval>();
			erByInterval = service.getAggregatedBrokerCommission();
			for (ERByInterval er : erByInterval) {
				hashMap.put(er.getSendingTime(), er.getAggregatedBrokerCommission());
			}
			return hashMap;
		}		
		
	// Get aggregated broker commission for all data in milliseconds.
		@RequestMapping(method=RequestMethod.GET, 
						value="/aggrBrokerCommissionMillis", 
						headers="Accept=application/json")
	public HashMap<Long, Double> getAggrBrokerCommissionInMilliseconds() {
		HashMap<Long, Double> hashMap = new HashMap<>();	
		List<ERByInterval> erByInterval = new ArrayList<ERByInterval>();
		erByInterval = service.getAggregatedBrokerCommission();
		for (ERByInterval er : erByInterval) {
			hashMap.put(millisecondsConverter(er.getSendingTime()), er.getAggregatedBrokerCommission());
		}
		return hashMap;
	}	
		
	// Get aggregated trade costs for all data based on stock type.
	@RequestMapping(method=RequestMethod.GET, 
			        value="/aggrTradeCosts", 
				    headers="Accept=application/json")
	public HashMap<String, ERByAccount> getAggrTradeCosts() {
		HashMap<String, ERByAccount> tradeCostHashMap = new HashMap<>();
		List<ERByAccount> erByAccount = new ArrayList<ERByAccount>();
		erByAccount = service.getTradeCostByAccount();
		for (ERByAccount er : erByAccount) {
			tradeCostHashMap.put(er.getAccount(), er);
		}
		return tradeCostHashMap;
	}		
 
	// Get all IOC hit rates for every time interval.
	@RequestMapping(method=RequestMethod.GET, 
			        value="/IOCHitRates", 
				    headers="Accept=application/json")
	public HashMap<LocalDateTime, Double> getIOCHitRates() {
		HashMap<LocalDateTime, Double> hashMap = new HashMap<>();
		List<ERByInterval> erByInterval = new ArrayList<ERByInterval>();
		erByInterval = service.getIOCHitRateByInterval();
		for (ERByInterval er : erByInterval) {
			hashMap.put(er.getSendingTime(), er.getIOCHitRate());
		}
		return hashMap;
	}	
	
	// Get all IOC hit rates for every time interval in milliseconds.
		@RequestMapping(method=RequestMethod.GET, 
				        value="/IOCHitRatesMillis", 
					    headers="Accept=application/json")
		public HashMap<Long, Double> getIOCHitRatesInMilliseconds() {
			HashMap<Long, Double> hashMap = new HashMap<>();
			List<ERByInterval> erByInterval = new ArrayList<ERByInterval>();
			erByInterval = service.getIOCHitRateByInterval();
			for (ERByInterval er : erByInterval) {
				hashMap.put(millisecondsConverter(er.getSendingTime()), er.getIOCHitRate());
			}
			return hashMap;
		}
		
	// Get all VWAPs for every time point specific to AAPL stock.
	@RequestMapping(method=RequestMethod.GET, 
			        value="/VWAPs/AAPL", 
				    headers="Accept=application/json")
	public HashMap<LocalDateTime, Double> getVWAPs() {
			HashMap<LocalDateTime, Double> hashMap = new HashMap<>();
			List<ERByInterval> erByInterval = new ArrayList<ERByInterval>();
			erByInterval = service.getAAPLVWAPByInterval();
			for (ERByInterval er : erByInterval) {
				hashMap.put(er.getSendingTime(), er.getAPPLVWAP());
			}
			return hashMap;
	}
	
	// Get all VWAPs for every time point specific to AAPL stock in milliseconds.
		@RequestMapping(method=RequestMethod.GET, 
				        value="/VWAPs/AAPL/Millis", 
					    headers="Accept=application/json")
		public HashMap<Long, Double> getVWAPsInMilliseconds() {
				HashMap<Long, Double> hashMap = new HashMap<>();
				List<ERByInterval> erByInterval = new ArrayList<ERByInterval>();
				erByInterval = service.getAAPLVWAPByInterval();
				for (ERByInterval er : erByInterval) {
					hashMap.put(millisecondsConverter(er.getSendingTime()), er.getAPPLVWAP());
				}
				return hashMap;
		}
	
	// Get all VWAPs for every time point specific to CSCOV stock.
	@RequestMapping(method=RequestMethod.GET, 
			        value="/VWAPs/CSCOV", 
				    headers="Accept=application/json")
	public HashMap<LocalDateTime, Double> getCSCOVVWAPs() {
		HashMap<LocalDateTime, Double> hashMap = new HashMap<>();
		List<ERByInterval> erByInterval = new ArrayList<ERByInterval>();
		erByInterval = service.getCSCOVWAPByInterval();
		for (ERByInterval er : erByInterval) {
			hashMap.put(er.getSendingTime(), er.getCSCOVWAP());
		}
		return hashMap;
	}
	
	// Get all VWAPs for every time point specific to CSCOV stock in milliseconds.
		@RequestMapping(method=RequestMethod.GET, 
				        value="/VWAPs/CSCOV/Millis", 
					    headers="Accept=application/json")
		public HashMap<Long, Double> getCSCOVVWAPsInMilliseconds() {
			HashMap<Long, Double> hashMap = new HashMap<>();
			List<ERByInterval> erByInterval = new ArrayList<ERByInterval>();
			erByInterval = service.getCSCOVWAPByInterval();
			for (ERByInterval er : erByInterval) {
				hashMap.put(millisecondsConverter(er.getSendingTime()), er.getCSCOVWAP());
			}
			return hashMap;
		}
	
	// Get all VWAPs for every time point specific to TSLA stock.
	@RequestMapping(method=RequestMethod.GET, 
	 	        value="/VWAPs/TSLA", 
			    headers="Accept=application/json")
	public HashMap<LocalDateTime, Double> getTSLAVWAPs() {
		HashMap<LocalDateTime, Double> hashMap = new HashMap<>();
		List<ERByInterval> erByInterval = new ArrayList<ERByInterval>();
		erByInterval = service.getTSLAVWAPByInterval();
		for (ERByInterval er : erByInterval) {
			hashMap.put(er.getSendingTime(), er.getTSLAVWAP());
		}
		return hashMap;
	}
	
	// Get all VWAPs for every time point specific to TSLA stock in milliseconds.
		@RequestMapping(method=RequestMethod.GET, 
		 	        value="/VWAPs/TSLA/Millis", 
				    headers="Accept=application/json")
		public HashMap<Long, Double> getTSLAVWAPsInMilliseconds() {
			HashMap<Long, Double> hashMap = new HashMap<>();
			List<ERByInterval> erByInterval = new ArrayList<ERByInterval>();
			erByInterval = service.getTSLAVWAPByInterval();
			for (ERByInterval er : erByInterval) {
				hashMap.put(millisecondsConverter(er.getSendingTime()), er.getTSLAVWAP());
			}
			return hashMap;
		}
	
	private static long millisecondsConverter(LocalDateTime lt){
        return Timestamp.valueOf(lt).getTime();
    }

		
}