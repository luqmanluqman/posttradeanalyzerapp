package com.entities;

import javax.persistence.*;

@Entity
@Table(name = "er_by_account")
public class ERByAccount {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private String account;
    private double AAPL;
    private double CSCO;
    private double TSLA;
    private double totalCost;

    public ERByAccount(){
        super();
    }

    public ERByAccount(String account, double AAPL, double CSCO, double TSLA, double totalCost){
        this.account = account;
        this.AAPL = AAPL;
        this.CSCO = CSCO;
        this.TSLA = TSLA;
        this.totalCost = totalCost;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public double getAAPL() {
        return AAPL;
    }

    public void setAAPL(double AAPL) {
        this.AAPL = AAPL;
    }

    public double getCSCO() {
        return CSCO;
    }

    public void setCSCO(double CSCO) {
        this.CSCO = CSCO;
    }

    public double getTSLA() {
        return TSLA;
    }

    public void setTSLA(double TSLA) {
        this.TSLA = TSLA;
    }

    public double getTotalCost() {
        return this.totalCost;
    }

    public void setTotalCost(double totalCost) {
        this.totalCost = totalCost;
    }

}
