package com.entities;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table(name = "er_by_interval")
public class ERByInterval {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private LocalDateTime sendingTime;
    private double brokerCommission;
    private double aggregatedBrokerCommission;
    private double IOCHitRate;
    private double APPLVWAP;
    private double CSCOVWAP;
    private double TSLAVWAP;

    public ERByInterval(){
        super();
    }

    public ERByInterval(LocalDateTime sendingTime, double brokerCommission, double aggregatedBrokerCommission, double IOCHitRate, double AAPLVWAP, double CSCOVWAP, double TSLAVWAP) {
        this.sendingTime = sendingTime;
        this.brokerCommission = brokerCommission;
        this.aggregatedBrokerCommission = aggregatedBrokerCommission;
        this.IOCHitRate = IOCHitRate;
        this.APPLVWAP = AAPLVWAP;
        this.CSCOVWAP = CSCOVWAP;
        this.TSLAVWAP = TSLAVWAP;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public LocalDateTime getSendingTime() {
        return sendingTime;
    }

    public void setSendingTime(LocalDateTime sendingTime) {
        this.sendingTime = sendingTime;
    }

    public double getBrokerCommission() {
        return brokerCommission;
    }

    public void setBrokerCommission(double brokerCommission) {
        this.brokerCommission = brokerCommission;
    }

    public double getAggregatedBrokerCommission() {
        return aggregatedBrokerCommission;
    }

    public void setAggregatedBrokerCommission(double aggregatedBrokerCommission) {
        this.aggregatedBrokerCommission = aggregatedBrokerCommission;
    }

    public double getIOCHitRate() {
        return IOCHitRate;
    }

    public void setIOCHitRate(double IOCHitRate) {
        this.IOCHitRate = IOCHitRate;
    }

    public double getAPPLVWAP() {
        return APPLVWAP;
    }

    public void setAPPLVWAP(double APPLVWAP) {
        this.APPLVWAP = APPLVWAP;
    }

    public double getCSCOVWAP() {
        return CSCOVWAP;
    }

    public void setCSCOVWAP(double CSCOVWAP) {
        this.CSCOVWAP = CSCOVWAP;
    }

    public double getTSLAVWAP() {
        return TSLAVWAP;
    }

    public void setTSLAVWAP(double TSLAVWAP) {
        this.TSLAVWAP = TSLAVWAP;
    }


}
