package com.entities;

import java.time.LocalTime;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "execution_report")
public class ExecutionReport
{
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	private LocalTime sendingTime;
	private String symbol;
	private int orderQty;
	private int lastShares;
	private float lastPx;
	private int cumQty;
	private String account;
	private String timeInForce;
	
	public ExecutionReport()
	{
		super();
	}

	public ExecutionReport(LocalTime localTime, String symbol, int orderQty, int lastShares,
			float lastPx, int cumQty, String account, String timeInForce) {
		this.sendingTime = localTime;
		this.symbol = symbol;
		this.orderQty = orderQty;
		this.lastShares = lastShares;
		this.lastPx = lastPx;
		this.cumQty = cumQty;
		this.account = account;
		this.timeInForce = timeInForce;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public LocalTime getSendingTime() {
		return sendingTime;
	}

	public void setSendingTime(LocalTime sendingTime) {
		this.sendingTime = sendingTime;
	}

	public String getSymbol() {
		return symbol;
	}

	public void setSymbol(String symbol) {
		this.symbol = symbol;
	}

	public int getOrderQty() {
		return orderQty;
	}

	public void setOrderQty(int orderQty) {
		this.orderQty = orderQty;
	}

	public int getLastShares() {
		return lastShares;
	}

	public void setLastShares(int lastShares) {
		this.lastShares = lastShares;
	}

	public float getLastPx() {
		return lastPx;
	}

	public void setLastPx(float lastPx) {
		this.lastPx = lastPx;
	}

	public int getCumQty() {
		return cumQty;
	}

	public void setCumQty(int cumQty) {
		this.cumQty = cumQty;
	}

	public String getAccount() {
		return account;
	}

	public void setAccount(String account) {
		this.account = account;
	}

	public String getTimeInForce() {
		return timeInForce;
	}

	public void setTimeInForce(String timeInForce) {
		this.timeInForce = timeInForce;
	}
}