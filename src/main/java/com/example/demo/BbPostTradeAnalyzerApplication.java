package com.example.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.ComponentScan;

import com.service.TradeAnalysisService;

@SpringBootApplication
@ComponentScan("com.*")
@EntityScan(basePackages = {"com.*"})
public class BbPostTradeAnalyzerApplication {

	public static void main(String[] args) {
		ApplicationContext ac = SpringApplication.run(BbPostTradeAnalyzerApplication.class, args);

		// To populate data into database - all 3mils already populated - DO NOT UNCOMMENT!!!
//		ReadFileUsingByteBuffer readFileUsingByteBuffer = ac.getBean(ReadFileUsingByteBuffer.class);
//		readFileUsingByteBuffer.read("C:\\Users\\DELL 2010\\Desktop\\er0.log");
		
//				TradeAnalysisRepo repo = ac.getBean(TradeAnalysisRepo.class);
//				repo.retrieveAll();
//				List<ExecutionReport> er = repo.getAll();
//				System.out.println(er.size());
		
//		TradeAnalysisServiceImpl service = 
//		ac.getBean(TradeAnalysisService.class);
//		List<ERByInterval> aapl = service.getAAPLVWAPByInterval();
//		System.out.println(aapl.size());
	}
}