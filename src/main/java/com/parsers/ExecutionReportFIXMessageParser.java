package com.parsers;

import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.DatabaseHandling.DatabaseGenerator;
import com.entities.ExecutionReport;

import quickfix.MessageUtils;
 
@Component
public class ExecutionReportFIXMessageParser
{
	@Autowired
	private DatabaseGenerator generator;
		
	public void parse(List<String> fixMessages)
	{
		ArrayList<ExecutionReport> execReports = new ArrayList<>();
		
		for (String fixMessage : fixMessages)
		{
			if(fixMessage.contains(" "))
			{
    	  		fixMessage = fixMessage.replace(" ", "");
			}
			
			String s;
			s = MessageUtils.getStringField(fixMessage, 52);
			
			execReports.add(new ExecutionReport(
					getTime(s),
					MessageUtils.getStringField(fixMessage, 55),
					Integer.parseInt(Optional.ofNullable(MessageUtils.getStringField(fixMessage, 38)).orElse("0")),
					Integer.parseInt(Optional.ofNullable(MessageUtils.getStringField(fixMessage, 32)).orElse("0")),
					Float.parseFloat(Optional.ofNullable(MessageUtils.getStringField(fixMessage, 31)).orElse("0")),
					Integer.parseInt(Optional.ofNullable(MessageUtils.getStringField(fixMessage, 14)).orElse("0")),
					MessageUtils.getStringField(fixMessage, 1),
					MessageUtils.getStringField(fixMessage, 59)));
		}
		generator.addReports(execReports);
	}
	
	private static LocalTime getTime(String s)
	{
		LocalTime time = LocalTime.parse(s);
		
		return time;
	}	
}