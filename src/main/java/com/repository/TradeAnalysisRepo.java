package com.repository;

import com.entities.ERByAccount;
import com.entities.ERByInterval;
import com.entities.ExecutionReport;

import java.util.List;

public interface TradeAnalysisRepo
{
	public void retrieveAll();

	public List<ExecutionReport> getAll();

	public List<ERByInterval> getBrokerCommission();

	public List<ERByInterval> getAggregatedBrokerCommission();

	public List<ERByInterval> getIOCHitRate();

	public List<ERByInterval> getAPPLVWAP();

	public List<ERByInterval> getCSCOVWAP();

	public List<ERByInterval> getTSLAVWAP();

	public List<ERByAccount> getByAccount();
}