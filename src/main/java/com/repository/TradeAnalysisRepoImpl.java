package com.repository;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import org.springframework.stereotype.Repository;

import com.entities.ERByAccount;
import com.entities.ERByInterval;
import com.entities.ExecutionReport;

@Repository
public class TradeAnalysisRepoImpl implements TradeAnalysisRepo
{
	@PersistenceContext
	private EntityManager entityManager;

	List<ExecutionReport> all = new ArrayList<ExecutionReport>();

	public List<ExecutionReport> getAll()
	{
		return all;
	}

	@Override
	public void retrieveAll()
	{
		TypedQuery<ExecutionReport> query = entityManager.createQuery("select e from ExecutionReport e", ExecutionReport.class);
		this.all = query.getResultList();
	}

	@Override
	public List<ERByInterval> getBrokerCommission()
	{
		List<Object[]> list = entityManager.createQuery("select e.sendingTime, e.brokerCommission from ERByInterval e").getResultList();
		List<ERByInterval> er = new ArrayList<ERByInterval>();
		ERByInterval interval = new ERByInterval();
		
		for(Object[] obj : list)
		{
			interval.setSendingTime((LocalDateTime) obj[0]);
			interval.setBrokerCommission((double) obj[1]);
			er.add(interval);
		}
		return er;
	}

	@Override
	public List<ERByInterval> getAggregatedBrokerCommission()
	{
		List<Object[]> list = entityManager.createQuery("select e.sendingTime, e.aggregatedBrokerCommission from ERByInterval e").getResultList();
		List<ERByInterval> er = new ArrayList<ERByInterval>();
		ERByInterval interval = new ERByInterval();
		
		for(Object[] obj : list)
		{
			interval.setSendingTime((LocalDateTime) obj[0]);
			interval.setAggregatedBrokerCommission((double) obj[1]);
			er.add(interval);
		}
		return er;
	}

	@Override
	public List<ERByInterval> getIOCHitRate()
	{
		List<Object[]> list = entityManager.createQuery("select e.sendingTime, e.IOCHitRate from ERByInterval e").getResultList();
		List<ERByInterval> er = new ArrayList<ERByInterval>();
		ERByInterval interval = new ERByInterval();
		
		for(Object[] obj : list)
		{
			interval.setSendingTime((LocalDateTime) obj[0]);
			interval.setIOCHitRate((double) obj[1]);
			er.add(interval);
		}
		return er;
	}

	@Override
	public List<ERByInterval> getAPPLVWAP()
	{
		List<Object[]> list = entityManager.createQuery("select e.sendingTime, e.APPLVWAP from ERByInterval e").getResultList();

		List<ERByInterval> er = new ArrayList<ERByInterval>();
		ERByInterval interval = new ERByInterval();
		
		for(Object[] obj : list)
		{
			interval.setSendingTime((LocalDateTime) obj[0]);
			interval.setAPPLVWAP((double) obj[1]);
			er.add(interval);
		}
		return er;
	}

	@Override
	public List<ERByInterval> getCSCOVWAP()
	{
		List<Object[]> list = entityManager.createQuery("select e.sendingTime, e.CSCOVWAP from ERByInterval e").getResultList();

		List<ERByInterval> er = new ArrayList<ERByInterval>();
		ERByInterval interval = new ERByInterval();
		
		for(Object[] obj : list)
		{
			interval.setSendingTime((LocalDateTime) obj[0]);
			interval.setCSCOVWAP((double) obj[1]);
			er.add(interval);
		}
		return er;
	}

	@Override
	public List<ERByInterval> getTSLAVWAP()
	{
		List<Object[]> list = entityManager.createQuery("select e.sendingTime, e.TSLAVWAP from ERByInterval e").getResultList();

		List<ERByInterval> er = new ArrayList<ERByInterval>();
		ERByInterval interval = new ERByInterval();
		
		for(Object[] obj : list)
		{
			interval.setSendingTime((LocalDateTime) obj[0]);
			interval.setTSLAVWAP((double) obj[1]);
			er.add(interval);
		}
		return er;
	}

	@Override
	public List<ERByAccount> getByAccount()
	{
		TypedQuery<ERByAccount> query = entityManager.createQuery("select e from ERByAccount e", ERByAccount.class);

		return query.getResultList();
	}
}