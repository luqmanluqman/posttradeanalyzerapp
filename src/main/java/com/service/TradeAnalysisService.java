package com.service;

import com.entities.ERByAccount;
import com.entities.ERByInterval;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;

public interface TradeAnalysisService {

    public List<ERByInterval> getBrokerCommissionByInterval();

    public List<ERByInterval> getAggregatedBrokerCommission();

    public List<ERByAccount> getTradeCostByAccount();

    public List<ERByInterval> getIOCHitRateByInterval();

    public List<ERByInterval> getAAPLVWAPByInterval();

    public List<ERByInterval> getCSCOVWAPByInterval();

    public List<ERByInterval> getTSLAVWAPByInterval();

}