package com.service;

import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.DatabaseHandling.DatabaseGenerator;
import com.entities.ERByAccount;
import com.entities.ERByInterval;
import com.repository.TradeAnalysisRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.entities.ExecutionReport;

import javax.annotation.PostConstruct;

@Service
public class TradeAnalysisServiceImpl implements TradeAnalysisService{
    public static final double COMMISSIONRATE = 0.05;
    public static final double TRADERATE = 0.02;
    public static final long INTERVAL = 60000;
    public static final String IOC = "3";
    public static final String AAPL = "AAPL";
    public static final String CSCO = "CSCO";
    public static final String TSLA = "TSLA";
    public static final LocalDate LOCALDATE =  LocalDate.of(2017,8,8);

    List<ExecutionReport> allData;
    Map<Long, List<ExecutionReport>> erByInterval;
    Map<String, List<ExecutionReport>> erByAccount;

    @Autowired
    public TradeAnalysisRepo repo;

    @Autowired
    public DatabaseGenerator dg;

    @PostConstruct
    public void TradeAnalysisServiceImpl(){
        this.allData = new ArrayList<>();
        repo.retrieveAll();
        if (repo.getAll() != null){
            this.allData = repo.getAll();
        }
        this.erByInterval = new HashMap<>();
        this.erByAccount = new HashMap<>();

        this.divideER();
        this.preCalculateERByInterval();
        this.preCalculateERByAccount();
    }

    private void divideER(){
        this.divideERByInterval();
        this.divideERByAccount();
    }

    private void preCalculateERByInterval(){
        double aggregatedBrokerCommission = 0;
        ArrayList<ERByInterval> erByInterval = new ArrayList<>();
        if (this.erByInterval != null) {
            for (Map.Entry<Long, List<ExecutionReport>> entry : this.erByInterval.entrySet()) {
                LocalDateTime ldt = localDateTimeConverter(entry.getKey() * INTERVAL);
                List<ExecutionReport> er = entry.getValue();
                double brokerCommission = calculateBrokerCommission(er);
                aggregatedBrokerCommission += brokerCommission;
                double IOCHitRate = calculateIOCHITRate(er);
                double AAPLVWAP = calculateVWAP(er, AAPL);
                double CSCOVWAP = calculateVWAP(er, CSCO);
                double TSLAVWAP = calculateVWAP(er, TSLA);
                ERByInterval erbi = new ERByInterval(ldt, brokerCommission, aggregatedBrokerCommission, IOCHitRate, AAPLVWAP, CSCOVWAP, TSLAVWAP);
                erByInterval.add(erbi);
            }

        } else {
            System.out.println("erByInterval is empty");
        }
        dg.addERByInterval(erByInterval);
    }

    private void preCalculateERByAccount(){
        ArrayList<ERByAccount> erByAccount = new ArrayList<>();
        if (this.erByAccount != null) {
            for (Map.Entry<String, List<ExecutionReport>> entry : this.erByAccount.entrySet()) {
                List<ExecutionReport> er = entry.getValue();
                double aapl = calculateTradeCost(er, AAPL);
                double csco = calculateTradeCost(er, CSCO);
                double tsla = calculateTradeCost(er, TSLA);
                double total = aapl + csco + tsla;
                ERByAccount erba = new ERByAccount(entry.getKey(), aapl, csco, tsla, total);
                erByAccount.add(erba);
            }

        } else {
            System.out.println("erByInterval is empty");
        }
        dg.addERByAccount(erByAccount);
    }

    private void divideERByInterval(){
        if (this.allData != null) {
            this.erByInterval = this.allData.stream().collect(Collectors.groupingBy(obj -> millisecondsConverter(obj.getSendingTime()) / INTERVAL));
        } else {
            System.out.println("Repo is null");
        }
    }

    private void divideERByAccount(){
        if (this.allData != null) {
            this.erByAccount = this.allData.stream().collect(Collectors.groupingBy(obj -> obj.getAccount()));
        } else {
            System.out.println("Repo is null");
        }
    }

    private double calculateBrokerCommission(List<ExecutionReport> er){
        return er.stream().mapToDouble(o -> calculateTradeAmount(o) * COMMISSIONRATE).sum();
    }

    private double calculateTradeCost(List<ExecutionReport> er, String symbol){
        return er.stream().filter(o -> o.getSymbol().equals(symbol)).mapToDouble(o -> calculateTradeAmount(o) * TRADERATE).sum();
    }

    private double calculateIOCHITRate(List<ExecutionReport> er){
        int orderSubmitted = 0;
        int orderExecuted = 0;
        orderSubmitted = er.stream().filter(o -> o.getTimeInForce().equals(IOC)).mapToInt(o -> o.getOrderQty()).sum();
        orderExecuted = er.stream().filter(o -> o.getTimeInForce().equals(IOC)).mapToInt(o -> o.getCumQty()).sum();
        if (orderSubmitted == 0) {
            return 0.0;
        } else {
            return (double) orderExecuted / orderSubmitted;
        }
    }

    private double calculateVWAP(List<ExecutionReport> er, String symbol){
        double tradeAmount = 0;
        double tradeVolume = 0;
        tradeAmount = er.stream().filter(o -> o.getSymbol().equals(symbol)).mapToDouble(o -> calculateTradeAmount(o)).sum();
        tradeVolume = er.stream().filter(o -> o.getSymbol().equals(symbol)).mapToDouble(o -> o.getLastShares()).sum();
        if (tradeVolume == 0){
            return 0.0;
        } else {
            return tradeAmount / tradeVolume;
        }
    }

    public List<ERByInterval> getBrokerCommissionByInterval(){
        return repo.getBrokerCommission();
    }

    public List<ERByInterval> getAggregatedBrokerCommission(){
        return repo.getAggregatedBrokerCommission();
    }
    public List<ERByAccount> getTradeCostByAccount(){
        return repo.getByAccount();
    }

    public List<ERByInterval> getIOCHitRateByInterval(){
        return repo.getIOCHitRate();
    }

    public List<ERByInterval> getAAPLVWAPByInterval(){
        return repo.getAPPLVWAP();
    }

    public List<ERByInterval> getCSCOVWAPByInterval(){
        return repo.getCSCOVWAP();
    }

    public List<ERByInterval> getTSLAVWAPByInterval(){
        return repo.getTSLAVWAP();
    }

    private static double calculateTradeAmount(ExecutionReport er) {
        return er.getLastPx() * er.getLastShares();
    }

    private static long millisecondsConverter(LocalTime lt){
        return Timestamp.valueOf(localTimeToLocalDateTime(lt)).getTime();
    }

    private static LocalDateTime localDateTimeConverter(long ms){
        return new Timestamp(ms).toLocalDateTime();
    }

    private static LocalDateTime localTimeToLocalDateTime(LocalTime lt){
        return lt.atDate(LOCALDATE);
    }

}
