-- <A> message
DROP TABLE IF EXISTS `log_on`;
CREATE TABLE `log_on` (
	`id` INT UNSIGNED NOT NULL,
	`begin_string` varchar(30),
	`sending_time` datetime(3),
	`check_sum` varchar(30),
	`target_comp_id` varchar(30),
	`reset_seq_num_flag` char(1),
	`heart_bt_int` int(11),
	`encrypt_method` char(1),
	`body_length` int(11),
	`sender_comp_id` varchar(30),
	`msg_seq_num` int(11)
	PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4

-- <D> message
DROP TABLE IF EXISTS `order_single`;
CREATE TABLE `order_single` (
 	`id` INT UNSIGNED NOT NULL,
 	`begin_string` varchar(30),
 	`body_length` int(11),
 	`msg_seq_num` int(11),
 	`sender_comp_id` varchar(30),
 	`sending_time` datetime(3),
 	`target_comp_id` varchar(30),
 	`account` varchar(30),
 	`cl_order_id` varchar(30),
 	`handl_inst` char(1),
 	`order_qty` decimal(3,0), --value is solely 100, it could be decimal in data specification
 	`order_type` char(1),
 	`price` decimal(3,0), --values are only 3 digits, it could be decimal in data specification
 	`side` char(1),
 	`symbol` varchar(30),
 	`time_in_force` char(1),
 	`check_sum` varchar(30),
 	PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- <8> message
DROP TABLE IF EXISTS `execution_report`;
CREATE TABLE `execution_report` (
 	`id` INT UNSIGNED NOT NULL,
	`cl_ord_id` varchar(30),
	`price` decimal(3,0),
	`ord_status` char(1),
	`time_in_force` char(1),
	`last_px` decimal(7,4), --values range from 0.0 to xxx.0000
	`account` varchar(30),
	`sender_comp_id` varchar(30),
	`order_id` varchar(30),
	`check_sum` varchar(30),
	`leaves_qty` decimal(3,0), --values are 0 or 100
	`orig_cl_ord_id` varchar(30),
	`last_shares` decimal(3,0), --values are 0 or 100
	`msg_seq_num` int(11),
	`ord_type` char(1),
	`exec_type` char(1),
	`order_qty` decimal(6,2),
	`symbol` varchar(30),
	`sending_time` datetime(3),
	`side` char(1),
	`begin_string` varchar(30),
	`exec_broker` varchar(30),
	`cum_qty` decimal(3,0), --values are 0 or 100
	`target_comp_id` varchar(30),
	`text` varchar(30),
	`exec_trans_type` char(1),
	`body_length` int(11),
	`exec_id` varchar(30),
	`transact_time` datetime(3),
	`avg_px` varchar(8), --values range from 0.0 to xxx.0000 as well as 'INET' and 'NEU'
	PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- <0> message
DROP TABLE IF EXISTS `heartbeat`;
CREATE TABLE `heartbeat` (
 	`id` INT UNSIGNED NOT NULL,
	`begin_string` varchar(30),
	`sending_time` datetime(3),
	`check_sum` varchar(30),
	`test_req_id` varchar(30),
	`target_comp_id` varchar(30),
	`body_length` int(11),
	`sender_comp_id` varchar(30),
	`msg_seq_num` int(11),
	PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- <1> message
DROP TABLE IF EXISTS `test_request`;
CREATE TABLE `test_request` (
 	`id` INT UNSIGNED NOT NULL,
 	`begin_string` varchar(30),
	`sending_time` datetime(3),
	`check_sum` int(11),
	`test_req_id` varchar(30),
	`target_comp_id` varchar(30),
	`body_length` int(11),
	`sender_comp_id` varchar(30),
	`msg_seq_num` int(11),
	PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- <3> message
DROP TABLE IF EXISTS `reject`;
CREATE TABLE `reject` (
 	`id` INT UNSIGNED NOT NULL,
 	`begin_string` varchar(30),
	`sending_time` datetime(3),
	`ref_seq_num` int(11),
	`target_comp_id` varchar(30),
	`text` varchar(30),
	`check_sum` int(11),
	`sender_comp_id` varchar(30),
	`body_length` int(11),
	`session_reject_reason` varchar(2),
	`ref_msg_type` varchar(30),
	`msg_seq_num` int(11),
	PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- <5> message
DROP TABLE IF EXISTS `logout`;
CREATE TABLE `logout` (
 	`id` INT UNSIGNED NOT NULL,
 	`begin_string` varchar(30),
	`sending_time` datetime(3),
	`check_sum` int(11),
	`target_comp_id` varchar(30),
	`body_length` int(11),
	`sender_comp_id` varchar(30),
	`msg_seq_num` int(11),
	PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;