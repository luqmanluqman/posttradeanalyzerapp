package com.DatabaseHandling;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

import java.time.LocalTime;
import java.util.ArrayList;

import org.junit.Test;

import com.entities.ExecutionReport;

public class DatabaseGeneratorTest
{
	@Test
	public void testAddReports_SuccessfullyAdded() {
		ArrayList<ExecutionReport> reports = new ArrayList<>();
		
		ExecutionReport er = new ExecutionReport(LocalTime.now(), "INET", 100, 212, 354, 100, "TSLA", "2");
		boolean erB = reports.add(er);
		
		assertThat(erB, is(equalTo(true)));
	}
	
}