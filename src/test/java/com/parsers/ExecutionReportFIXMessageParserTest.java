package com.parsers;

import static org.junit.Assert.assertEquals;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;

import org.junit.Test;

public class ExecutionReportFIXMessageParserTest
{
	@Test
	public void testSuccessfulModifyDateString() {
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Calendar calendarTest = new GregorianCalendar(2018,8,20,13,30,30);
		String newDate = dateFormat.format(calendarTest.getTime());
		
		// Creating new GregorianCalendar begins from 0, therefore 8 -> 9
		String toCompare = "2018-09-20 13:30:30";
		assertEquals(newDate, toCompare);
	}
}