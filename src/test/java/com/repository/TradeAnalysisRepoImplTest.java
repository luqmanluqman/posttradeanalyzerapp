package com.repository;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import com.entities.ERByInterval;
import com.entities.ExecutionReport;

public class TradeAnalysisRepoImplTest
{
	@InjectMocks
	TradeAnalysisRepoImpl repo;
	
	@Mock
	EntityManager em;
	
	@Test
	public void testQueryObject_SuccessfulQueryOfFirstObject() {
		Object[] object = new Object[] {"1970-01-01 06:57:16", 353.492455858748};
		List<Object[]> dummyList = new ArrayList<>();
		dummyList.add(object);
		
		assertEquals(dummyList.get(0)[0], "1970-01-01 06:57:16");
	}
	
	@Test
	public void testQueryObject_SuccessfulQueryOfSecondObject() {
		Object[] object = new Object[] {"1970-01-01 06:57:16", 353.492455858748};
		List<Object[]> dummyList = new ArrayList<>();
		dummyList.add(object);
		
		assertEquals(dummyList.get(0)[1], 353.492455858748);
	}
}