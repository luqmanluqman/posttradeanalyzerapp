package com.service;

import static org.junit.Assert.*;
import static org.mockito.Mockito.when;

import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import com.entities.ERByInterval;
import com.entities.ExecutionReport;
import com.repository.TradeAnalysisRepoImpl;

public class TradeAnalysisServiceImplTest {
	
	@Mock
	TradeAnalysisRepoImpl repo;
	
	@InjectMocks
	TradeAnalysisServiceImpl serv;

	@Test
	public void testSumFromStreamBrokerCommission_CorrectlyAdded() {
		ExecutionReport er = new ExecutionReport(LocalTime.now(), "AAPL", 10, 100, 12.44f, 100, "C3PO", "3");
		ExecutionReport er2 = new ExecutionReport(LocalTime.now(), "TSLA", 10, 100, 12.44f, 100, "C3PO", "3");
		double sum = (er.getCumQty() + er2.getCumQty())*0.02;
		assertEquals(sum, 4, 0.0000001);
	}
	
	@Test
	public void testSumFromStreamTradeCost_CorrectlyAdded() {
		ExecutionReport er = new ExecutionReport(LocalTime.now(), "AAPL", 10, 100, 12.44f, 100, "C3PO", "3");
		ExecutionReport er2 = new ExecutionReport(LocalTime.now(), "TSLA", 10, 100, 12.44f, 100, "C3PO", "3");
		double sum = (er.getCumQty() + er2.getCumQty())*0.05;
		assertEquals(sum, 10, 0.0000001);
	}

	@Test
	public void testCalcOrderSubmitted_CorrectlyAdded() {
		ExecutionReport er = new ExecutionReport(LocalTime.now(), "AAPL", 100, 100, 12.44f, 100, "C3PO", "3");
		int orderSub = er.getOrderQty();
		assertEquals(orderSub,100);
	}

	@Test
	public void testCalcOrderExecuted_CorrectlyAdded() {
		ExecutionReport er = new ExecutionReport(LocalTime.now(), "AAPL", 100, 100, 12.44f, 100, "C3PO", "3");
		int orderEx = er.getLastShares();
		assertEquals(orderEx,100);
	}
	
	@Test
	public void testOICHitRate_CorrectlyAdded() {
		ExecutionReport er = new ExecutionReport(LocalTime.now(), "AAPL", 100, 100, 12.44f, 100, "C3PO", "3");
		int orderEx = er.getLastShares();
		int orderSub = er.getOrderQty();
		
		assertEquals(orderEx/orderSub, 1);
	}
	
	@Test
	public void testCalcTradeAmt_CorrectlyAdded() {
		ExecutionReport er = new ExecutionReport(LocalTime.now(), "AAPL", 100, 100, 12.44f, 100, "C3PO", "3");
		float tradedAmt = er.getLastPx();
		assertEquals(tradedAmt, 12.44, 0.00001);
	}
	
	@Test
	public void testCalcTradeQty_CorrectlyAdded() {
		ExecutionReport er = new ExecutionReport(LocalTime.now(), "AAPL", 100, 100, 12.44f, 100, "C3PO", "3");
		int tradedQty = er.getLastShares();
		assertEquals(tradedQty, 100);
	}
	
	@Test
	public void testCalcVWAP_CorrectlyAdded() {
		ExecutionReport er = new ExecutionReport(LocalTime.now(), "AAPL", 100, 100, 12.44f, 100, "C3PO", "3");
		int tradedQty = er.getLastShares();
		float tradedAmt = er.getLastPx();
		
		assertEquals(tradedAmt/tradedQty, 0.1244, 0.00001);
	}

}
